<?php

if(islogged()){
    header('Location: ' . $address . 'home');
}

$pagedata['menu'] = load_tmp('menufull' ,'base');
$pagedata['header'] = load_tmp('header' ,'base');
if ($config['register'] === 1){$pagedata['registernow'] = load_tmp('registernow' ,'index');} elseif ($config['register'] === 0){$pagedata['registernow'] = load_tmp('registernow-f' ,'index');}
if ($config['login'] === 1){$pagedata['login'] = load_tmp('login' ,'index');} elseif ($config['login'] === 0){$pagedata['login'] = load_tmp('login-f' ,'index');}
$pagedata['footer'] = load_tmp('footer' ,'base');
$pagedata['spunsers'] = load_tmp('spunsers' ,'base');
$pagedata['contact'] = load_tmp('contact' ,'index');
$pagedata['thisp'] = load_tmp('thisp' ,'index');
$pagedata['aim'] = load_tmp('weaim' ,'index');
$pagedata['banner'] = load_tmp('banner' ,'index');
$pagedata['loadscroll'] = load_tmp('loadscroll' ,'base');

$pagedata['scripts'] = load_tmp('scripts' ,'base');

$pagedata['contbody'] = load_tmp('index' ,'index');

if ($lang['direction'] === 'ltr'){$pagedata['import'] = load_tmp('import' ,'base');} elseif ($lang['direction'] === 'rtl'){$pagedata['import'] = load_tmp('import-rtl' ,'base');}