<?php
include 'core.php';
if(!isloggeda()){
    header('Location: ' . $address . 'cpadmin');
}
if ($admin['adminone'] === $userid && $admin['passone'] === $pass) {
    $admini['type'] = 1;
    $admini['user'] = $admin['adminone'];
} elseif ($admin['admintwo'] === $userid && $admin['passtwo'] === $pass) {
    $admini['type'] = 2;
    $admini['user'] = $admin['admintwo'];
} elseif ($admin['adminthree'] === $userid && $admin['passthree'] === $pass) {
    $admini['type'] = 3;
    $admini['user'] = $admin['admintwo'];
}
if (isset($_GET['username']) && isset($_GET['pass'])){
    $myfile = fopen('../includes/admins.php' ,'w');
    if ($admini['type'] === 1) {
        if (isset($_GET['pass'])){
            $text = '<?php

$admin = array(
    "adminone" => "' . $_GET['username'] . '",
    "passone" => "' . md5($_GET['pass']) . '",
    "admintwo" => "' . $admin['admintwo'] . '",
    "passtwo" => "' . $admin['passtwo'] . '",
    "adminthree" => "' . $admin['adminthree'] . '",
    "passthree" => "' . $admin['passthree'] . '",
);';
        }
    } elseif ($admini['type'] === 2){
        if (isset($_GET['pass'])){
            $text = '<?php

$admin = array(
    "adminone" => "' . $admin['adminone'] . '",
    "passone" => "' . $admin['passone'] . '",
    "admintwo" => "' . $_GET['username'] . '",
    "passtwo" => "' . md5($_GET['pass']) . '",
    "adminthree" => "' . $admin['adminthree'] . '",
    "passthree" => "' . $admin['passthree'] . '",
);';
        }
    } elseif ($admini['type'] === 2){
        if (isset($_GET['pass'])){
            $text = '<?php

$admin = array(
    "adminone" => "' . $admin['adminone'] . '",
    "passone" => "' . $admin['passone'] . '",
    "admintwo" => "' . $admin['admintwo'] . '",
    "passtwo" => "' . $admin['passtwo'] . '",
    "adminthree" => "' . md5($_GET['pass']) . '",
    "passthree" => "' . $_GET['username'] . '",
);';
        }
    }
    fwrite($myfile, $text);
    fclose($myfile);
    header('Location: ' . $address . 'cpadmin');
}
if (isset($_GET['onwork']) && isset($_GET['register']) && isset($_GET['login']) && isset($_GET['download']) && isset($_GET['answer']) && isset($_GET['file']) && isset($_GET['scoring'])) {
    $myfile = fopen('../includes/config.php', 'w');
    $text = '<?php

$config = array(
    "db_name" => "' . $config['db_name'] . '",
    "db_user" => "' . $config['db_user'] . '",
    "db_password" => "' . $config['db_password'] . '",
    "db_host" => "' . $config['db_host'] . '",
    
    "address" => "' . $config['address'] . '",
    "addres" => "' . $config['addres'] . '",
    
    "template" => "' . $config['template'] . '",
    
    "onwork" => ' . $_GET['onwork'] . ',

    "register" => ' . $_GET['register'] . ',
    "login" => ' . $_GET['login'] . ',
    "downq" => ' . $_GET['download'] . ',
    "answer" => ' . $_GET['answer'] . ',
    "scoring" => ' . $_GET['scoring'] . ',

    "filename" => "' . $_GET['file'] . '",

    "d_lang" => "' . $config['d_lang'] . '",
);';
    fwrite($myfile, $text);
    fclose($myfile);
    header('Location: ' . $address . 'cpadmin/adminset.php');
}
if (isset($_POST['sudoco']) && isset($_POST['sudoct'])){
    include '../includes/sudocodes.php';
    $sudoco = $_POST['sudoco'];
    $sudoct = md5($_POST['sudoct']);
    if ($sudoco === $sudococore && $sudoct === $sudoctcore){
        $conn->query("DELETE FROM `answers`");
        $conn->query("DELETE FROM `contacts`");
        $conn->query("DELETE FROM `jurors`");
        $conn->query("DELETE FROM `points`");
        $conn->query("DELETE FROM `rank`");
        $conn->query("DELETE FROM `users`");
    }
}
if (isset($_POST['ranging'])){
    include '../includes/sudocodes.php';
    $sudoco = $_POST['sudocor'];
    if ($sudoco === $sudococore){
        $conn->query("DELETE FROM `rank`");
    }
}
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>CP Admins</title>

    <link rel="shortcut icon" href="img/favicon.png">
    <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
    <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
    <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
    <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
    <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
    <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
    <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
    <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/plugins.css">

    <link rel="stylesheet" href="css/main.css">

    <link rel="stylesheet" href="css/themes.css">
    <script src="js/vendor/modernizr-3.3.1.min.js"></script>
</head>
<body>
<div id="page-wrapper" class="page-loading">
    <div class="preloader">
        <div class="inner">
            <div class="preloader-spinner themed-background hidden-lt-ie10"></div>
            <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
        </div>
    </div>
    <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
        <div id="sidebar">
            <div id="sidebar-brand" class="themed-background">
                <a href="<?php echo $address . 'cpadmin' ?>" class="sidebar-title">
                    <i class="fa fa-cube"></i> <span class="sidebar-nav-mini-hide">CP<strong>Admins</strong></span>
                </a>
            </div>
            <div id="sidebar-scroll">
                <div class="sidebar-content">
                    <ul class="sidebar-nav">
                        <li>
                            <a href="<?php echo $address . 'cpadmin' ?>"><i class="gi gi-group sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Jurors</span></a>
                        </li>
                        <li>
                            <a href="<?php echo $address . 'cpadmin/addjuror.php' ?>"><i class="fa fa-plus sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Add Juror</span></a>
                        <li class="sidebar-separator">
                            <i class="fa fa-ellipsis-h"></i>
                        </li>
                        <li>
                            <a href="<?php echo $address . 'cpadmin/adminset.php' ?>" class="active"><i class="gi gi-settings sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Setting</span></a>
                        </li>
                        <li>
                            <a href="<?php echo $address . 'cpadmin/contact.php' ?>"><i class="gi gi-conversation sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Tickets</span></a>
                        </li>
                        <li>
                            <a href="<?php echo $address . 'cpadmin/users.php' ?>"><i class="gi gi-group sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">All Users</span></a>
                        </li>
                        <li>
                            <a href="<?php echo $address . 'cpadmin/?pg=logout' ?>"><i class="hi hi-log_out sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">LogOut</span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="sidebar-extra-info" class="sidebar-content sidebar-nav-mini-hide">
                <div class="text-center">
                    <small>Crafted with <i class="fa fa-heart text-danger"></i></small><br>
                    <small><span id="year-copy"></span> &copy; <a href="<?php echo $address; ?>" target="_blank">Paradox</a></small>
                </div>
            </div>
        </div>
        <div id="main-container">
            <header class="navbar navbar-inverse navbar-fixed-top">
                <ul class="nav navbar-nav-custom">
                    <li>
                        <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                            <i class="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
                            <i class="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
                        </a>
                    </li>
                    <li class="hidden-xs animation-fadeInQuick">
                        <a href="<?php echo $address . 'cpadmin'; ?>"><strong>DASHBOARD</strong></a>
                    </li>
                </ul>
                <ul class="nav navbar-nav-custom pull-right">
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="img/placeholders/avatars/avatar.jpg" alt="avatar">
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li class="dropdown-header">
                                <strong><?php echo $userid ?></strong>
                            </li>
                            <li>
                                <a href="<?php echo $address . 'cpadmin/adminset.php' ?>">
                                    <i class="gi gi-settings fa-fw pull-right"></i>
                                    Setting
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $address . 'cpadmin/?pg=logout' ?>">
                                    <i class="hi hi-log_out fa-fw pull-right"></i>
                                    LogOut
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </header>
            <div id="page-content">
                <div class="content-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="header-section">
                                <h1>Setting</h1>
                            </div>
                        </div>
                        <div class="col-sm-6 hidden-xs">
                            <div class="header-section">
                                <ul class="breadcrumb breadcrumb-top">
                                    <li>Control Panel</li>
                                    <li><a href="<?php echo $address . 'cpadmin' ?>">Home</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block full">
                    <div class="block-title">
                        <h2>Change Informian</h2>
                    </div>
                    <form method="get" class="form-horizontal form-bordered" id="form">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="username">UserName</label>
                            <div class="col-md-9">
                                <input type="text" name="username" id="username" class="form-control" value="<?php echo $admini['user']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="pass">PassWord</label>
                            <div class="col-md-9">
                                <input type="password" name="pass" id="pass" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="repass">RePassWord</label>
                            <div class="col-md-9">
                                <input type="password" name="repass" id="repass" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="block full">
                    <div class="block-title">
                        <h2>Change Informian</h2>
                    </div>
                    <form method="get" class="form-horizontal form-bordered" id="form">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="onwork">ONWORK</label>
                            <div class="col-md-9">
                                <input type="text" name="onwork" id="onwork" class="form-control" value="<?php echo $config['onwork']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="register">REGISTER</label>
                            <div class="col-md-9">
                                <input type="text" name="register" id="register" class="form-control" value="<?php echo $config['register']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="login">LOGIN</label>
                            <div class="col-md-9">
                                <input type="text" name="login" id="login" class="form-control" value="<?php echo $config['login']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="download">DOWNLOAD</label>
                            <div class="col-md-9">
                                <input type="text" name="download" id="download" class="form-control" value="<?php echo $config['downq']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="answer">SUBMIT ANSWER</label>
                            <div class="col-md-9">
                                <input type="text" name="answer" id="answer" class="form-control" value="<?php echo $config['answer']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="file">FILE NAME</label>
                            <div class="col-md-9">
                                <input type="text" name="file" id="file" class="form-control" value="<?php echo $config['filename']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="scoring">SCORING</label>
                            <div class="col-md-9">
                                <input type="text" name="scoring" id="scoring" class="form-control" value="<?php echo $config['scoring']; ?>">
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="block full">
                    <div class="block-title">
                        <h2>DELETE DATA</h2>
                    </div>
                    <form method="post" class="form-horizontal form-bordered">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="sudoco">SCURITY CODE 1</label>
                            <div class="col-md-9">
                                <input type="password" name="sudoco" id="sudoco" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="sudoct">SCURITY CODE 2</label>
                            <div class="col-md-9">
                                <input type="password" name="sudoct" id="sudoct" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-effect-ripple btn-danger">DELETE ALL DATA IN DATABASE</button>
                            </div>
                        </div>
                    </form>
                    <form method="post" class="form-horizontal form-bordered">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="sudocor">SCURITY CODE</label>
                            <div class="col-md-9">
                                <input type="password" name="sudocor" id="sudocor" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <input type="hidden" name="ranking" value="deleting">
                                <button type="submit" class="btn btn-effect-ripple btn-danger">DELETE RANKING</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/app.js"></script>
<script src="js/readySetting.js"></script>
<script>$(function(){ ReadySettinga.init(); });</script>
</body>
</html>
