/*
 *  Document   : readyRegister.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Register page
 */

var ReadySetting = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Register form - Initialize Validation */
            $('#form').validate({
                errorClass: 'help-block animation-slideUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    if (e.closest('.form-group').find('.help-block').length === 2) {
                        e.closest('.help-block').remove();
                    } else {
                        e.closest('.form-group').removeClass('has-success has-error');
                        e.closest('.help-block').remove();
                    }
                },
                rules: {
                    'name': {
                        required: true,
                    },
                    'email': {
                        required: true,
                        email: true
                    },
                    'pass': {
                        required: true,
                        minlength: 5
                    },
                    'repass': {
                        required: true,
                        equalTo: '#pass'
                    }
                },
                messages: {
                    'name': 'Please enter your name',
                    'email': 'Please enter a valid email address',
                    'pass': {
                        required: 'Please provide a password',
                        minlength: 'Your password must be at least 5 characters long'
                    },
                    'repass': {
                        required: 'Please provide a password',
                        equalTo: 'Please enter the same password as above'
                    }
                }
            });
        }
    };
}();

var ReadySettinga = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Register form - Initialize Validation */
            $('#form').validate({
                errorClass: 'help-block animation-slideUp', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    if (e.closest('.form-group').find('.help-block').length === 2) {
                        e.closest('.help-block').remove();
                    } else {
                        e.closest('.form-group').removeClass('has-success has-error');
                        e.closest('.help-block').remove();
                    }
                },
                rules: {
                    'username': {
                        required: true,
                    },
                    'pass': {
                        minlength: 5
                    },
                    'repass': {
                        equalTo: '#pass'
                    }
                },
                messages: {
                    'username': 'Please enter your username',
                    'pass' : 'Your password must be at least 5 characters long',
                    'repass': 'Please enter the same password as above'
                }
            });
        }
    };
}();