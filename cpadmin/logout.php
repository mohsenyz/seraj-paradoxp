<?php

if (isset($_SESSION['juror_id']))
{
    unset($_SESSION['juror_id']);
}

if (isset($_SESSION['juror_pass']))
{
    unset($_SESSION['juror_pass']);
}

if (isset($_SESSION['admin_id']))
{
    unset($_SESSION['admin_id']);
}

if (isset($_SESSION['admin_pass']))
{
    unset($_SESSION['admin_pass']);
}