<?php
include 'core.php';
if(!isloggedj()){
    header('Location: ' . $address . 'cpadmin');
}
$jurorq = $conn->query("SELECT * FROM `jurors` WHERE id='$userid' AND password='$pass'");
$juror = $jurorq->fetch_array(MYSQLI_ASSOC);
if (isset($_GET['pointanso']) && isset($_GET['pointanstw']) && isset($_GET['pointanstr']) && isset($_GET['ansid'])){
    $pointo = $_GET['pointanso'];
    $pointtw = $_GET['pointanstw'];
    $pointtr = $_GET['pointanstr'];
    $id = $_GET['ansid'];
    $userqe = $conn->query("SELECT * FROM `points` WHERE `answerid`='$id' AND `jurorid`='$userid'");
    if($userqe->num_rows === 0){
        $query = $conn->query("INSERT INTO `points`(`answerid`, `answero`, `answertw`, `answertr`, `jurorid`) VALUES ('$id', '$pointo', '$pointtw', '$pointtr', '$userid')");
    }
}
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>CP Jurors</title>

        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">

        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/plugins.css">

        <link rel="stylesheet" href="css/main.css">

        <link rel="stylesheet" href="css/themes.css">
        <script src="js/vendor/modernizr-3.3.1.min.js"></script>
    </head>
    <body>
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>
                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                <div id="sidebar">
                    <div id="sidebar-brand" class="themed-background">
                        <a href="<?php echo $address . 'cpadmin' ?>" class="sidebar-title">
                            <i class="fa fa-cube"></i> <span class="sidebar-nav-mini-hide">CP<strong>Jurors</strong></span>
                        </a>
                    </div>
                    <div id="sidebar-scroll">
                        <div class="sidebar-content">
                            <ul class="sidebar-nav">
                                <li>
                                    <a href="<?php echo $address . 'cpadmin' ?>"><i class="fa fa-dashboard sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Scoring</span></a>
                                </li>
                                <li>
                                    <a href="<?php echo $address . 'cpadmin/allteams.php' ?>" class="active"><i class="gi gi-group sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">All Teams</span></a>
                                </li>
                                <li class="sidebar-separator">
                                    <i class="fa fa-ellipsis-h"></i>
                                </li>
                                <li>
                                    <a href="<?php echo $address . 'cpadmin/setting.php' ?>"><i class="gi gi-settings sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Setting</span></a>
                                </li>
                                <li>
                                    <a href="<?php echo $address . 'cpadmin/?pg=logout' ?>"><i class="hi hi-log_out sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">LogOut</span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="sidebar-extra-info" class="sidebar-content sidebar-nav-mini-hide">
                        <div class="text-center">
                            <small>Crafted with <i class="fa fa-heart text-danger"></i></small><br>
                            <small><span id="year-copy"></span> &copy; <a href="<?php echo $address; ?>" target="_blank">Paradox</a></small>
                        </div>
                    </div>
                </div>
                <div id="main-container">
                    <header class="navbar navbar-inverse navbar-fixed-top">
                        <ul class="nav navbar-nav-custom">
                            <li>
                                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                    <i class="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
                                    <i class="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
                                </a>
                            </li>
                            <li class="hidden-xs animation-fadeInQuick">
                                <a href="<?php echo $address . 'cpadmin'; ?>"><strong>DASHBOARD</strong></a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav-custom pull-right">
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="img/placeholders/avatars/avatar.jpg" alt="avatar">
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li class="dropdown-header">
                                        <strong><?php echo $juror['name'] ?></strong>
                                    </li>
                                    <li>
                                        <a href="<?php echo $address . 'cpadmin/setting.php' ?>">
                                            <i class="gi gi-settings fa-fw pull-right"></i>
                                            Setting
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $address . 'cpadmin/?pg=logout' ?>">
                                            <i class="hi hi-log_out fa-fw pull-right"></i>
                                            LogOut
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </header>
                    <div id="page-content">
                        <div class="content-header">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="header-section">
                                        <h1>Scores</h1>
                                    </div>
                                </div>
                                <div class="col-sm-6 hidden-xs">
                                    <div class="header-section">
                                        <ul class="breadcrumb breadcrumb-top">
                                            <li>Control Panel</li>
                                            <li><a href="<?php echo $address . 'cpadmin' ?>">Home</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block full">
                            <div class="block-title">
                                <h2>rating</h2>
                            </div>
                            <div class="table-responsive">
                                <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                                    <thead>
                                    <tr>
                                        <th class="text-center" style="width: 50px;">Team Code</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Answer One</th>
                                        <th class="text-center">Answer Two</th>
                                        <th class="text-center">Answer three</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $userq = $conn->query("SELECT * FROM `users` WHERE code IN (SELECT `teamcode` FROM `answers` WHERE id IN (SELECT `answerid` FROM `points` WHERE `jurorid`='$userid')) ORDER BY id DESC LIMIT 20");
                                    while ($users = $userq->fetch_array(MYSQLI_ASSOC)) {
                                        $answeri = $conn->query("SELECT * FROM `points` WHERE answerid IN (SELECT id FROM `answers` WHERE `teamcode`='" . $users['code'] . "')");
                                        $answerpo = $answeri->fetch_array(MYSQLI_ASSOC);
                                    ?>
                                        <tr>
                                            <td class="text-center"><?php echo $users['code'] ?></td>
                                            <td dir="rtl" class="text-right"><strong><?php echo $users['nameo'] ?></strong></td>
                                            <td class="text-center"><?php echo $answerpo['answero'] ?></td>
                                            <td class="text-center"><?php echo $answerpo['answertw'] ?></td>
                                            <td class="text-center"><?php echo $answerpo['answertr'] ?></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>
    </body>
</html>