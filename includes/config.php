<?php

$config = array(
    "db_name" => "paradox",
    "db_user" => "root",
    "db_password" => "",
    "db_host" => "localhost",
    
    "address" => "http://localhost/paradoxp/",
    "addres" => "localhost",
    
    "template" => "basic",
    
    "onwork" => 0,

    "register" => 1,
    "login" => 1,
    "downq" => 1,
    "answer" => 1,
    "scoring" => 1,

    "filename" => "ParadoxPoblems-1st",

    "d_lang" => "pr",
);